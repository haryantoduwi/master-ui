<?php
	class Administrator extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model(array(''));
			if($this->session->userdata('status') != "login"){
				redirect(base_url('Login'));	
			}
		}
		function index(){
			//$this->load->view('admin');
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php');
			$this->load->view('master/mainsidebar.php');
			$this->load->view('master/maincontent.php');
			//$this->load->view('copyright.php');
			$this->load->view('master/footer.php');
		}
		/*
		function penilaiandp3(){
			//$unit=$this->session->userdata('unit');
			
			$data['pegawai']=$this->mdp3->getdatapegawai()->result();
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php');
			$this->load->view('master/mainsidebar.php');
			$this->load->view('dp3/penilaiandp3.php',$data);
			//$this->load->view('copyright.php');
			//$this->load->view('footer.php');
			$this->load->view('master/footerev.php');	
		}
		function simpanpenilaian(){
			$data['kesetiaan']=$this->input->post('kesetiaan');
			$data['prestasi']=$this->input->post('prestasikerja');
			$data['tanggungjwb']=$this->input->post('tanggungjawab');
			$data['ketaatan']=$this->input->post('ketaatan');
			$data['kejujuran']=$this->input->post('kejujuran');
			$data['kerjasama']=$this->input->post('kerjasama');
			$data['prakarsa']=$this->input->post('prakarsa');
			$data['kepemimpinan']=$this->input->post('kepemimpinan');
			$data['periodeawal']=$this->input->post('periodeawal');
			$data['periodeakhir']=$this->input->post('periodeakhir');
			$data['idpegawai']=$this->input->post('idpegawai');
			$this->mdp3->simpandatadp3($data);
			//echo $data['id_peg'];
			redirect(site_url('cadmin'));
			//echo $id;
		}
		function tampildp3(){
			$id=$this->session->userdata('id_user');
			$data['dp3']=$this->mdp3->tampildatadp3($id)->result();
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php');
			$this->load->view('master/mainsidebar.php');
			$this->load->view('dp3/tampildp3.php',$data);
			//$this->load->view('copyright.php');
			$this->load->view('master/footerev.php');
		}
		function lihatnilai(){
			$id=$this->session->userdata('id_user');
			$data['dp3']=$this->mdp3->lihatnilai($id)->result();
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php');
			$this->load->view('master/mainsidebar.php');
			$this->load->view('pegawai/lihatnilai.php',$data);
			//$this->load->view('copyright.php');
			$this->load->view('master/footerev.php');
			//$this->load->view('footerev.php');
		}
		function hapusdp3($idpenilaian){
			$this->mdp3->hapusdatadp3($idpenilaian);
			//echo $idpenilaian;
			redirect(site_url('cadmin/tampildp3'));
		}
		function addpegawai(){
			$data['pegawai']=$this->mdp3->getdatapegawai()->result();
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php');
			$this->load->view('master/mainsidebar.php');
			$this->load->view('pegawai/addpegawai.php',$data);
			//$this->load->view('copyright.php');	
			$this->load->view('master/footerev.php');
		}
		function simpanpegawai(){
			$data['idadmin']=$this->input->post('iduser');
			$data['nama']=$this->input->post('nama');
			$data['nik']=$this->input->post('nik');
			$data['pangkat']=$this->input->post('pangkat');
			$data['jabatan']=$this->input->post('jabatan');
			$data['unit']=$this->input->post('unit');
			$this->mpegawai->simpanpegawai($data);
			redirect(site_url('cadmin/addpegawai'));
		}
		function lihatpegterdaftar(){
			$id=$this->session->userdata('id_user');
			$data['res']=$this->mpegawai->tampil($id)->result();
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php');
			$this->load->view('master/mainsidebar.php');
			$this->load->view('pegawai/tampilpeg.php',$data);
			//$this->load->view('copyright.php');
			$this->load->view('master/footer.php');
		}
		function mulainilai($idpeg){
			$data['pegawai']=$this->mpegawai->getdatapeg($idpeg)->row();
			$this->load->view('master/header.php');
			$this->load->view('master/mainheader.php');
			$this->load->view('master/mainsidebar.php');
			$this->load->view('pegawai/penilaianpegawai.php',$data);
			$this->load->view('master/footer.php');
		}
		function penilaian($idnilai){
			$data['nilai']=$this->mdp3->getdatanilai($idnilai)->row();
			$this->load->view('header.php');
			$this->load->view('master/mainheader.php');
			$this->load->view('master/mainsidebar.php');
			$this->load->view('dp3/penilaianpegawai.php',$data);
			$this->load->view('master/footer.php');
		}
		*/
	}
?>