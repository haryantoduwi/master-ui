<?php

	class Login extends CI_Controller
	{
	
		function __construct(){
			parent::__construct();
			$this->load->model('Mlogin');
		}
		function index(){
			//CAPTCHA
            // load codeigniter captcha helper
            $this->load->helper('captcha');
 
            $vals = array(
                'img_path'	 => './captcha/',
                'img_url'	 => base_url().'captcha/',
                'img_width'	 => '200',
                'img_height' => 30,
                'border' => 0, 
                'expiration' => 7200
            );
 
            // create captcha image
            $cap = create_captcha($vals);
 
            // store image html code in a variable
            $data['image'] = $cap['image'];
 
            // store the captcha word in a session
            $this->session->set_userdata('mycaptcha', $cap['word']);
 
            //$this->load->view('registrasi_view.php', $data);				
			//SET STATUS AWAL NULL
			$data['statuslogin']='';
			$this->load->view('login',$data);

		}
		function aksi_login(){
			$username=$this->input->post('username');
			//$password=md5($this->input->post('password'));
			$password=$this->input->post('password');
			$cek=$this->Mlogin->cek($username,$password);
			if($cek->num_rows() == 1 ){
				foreach($cek->result() as $data){
					$sess_data['id']=$data->id;
					$sess_data['nama']=$data->nama;
					$sess_data['username']=$data->username;
					$sess_data['password']=$data->password;
					$sess_data['level']=$data->level;
					$sess_data['tgl']=$data->tgl;
					$sess_data['status']="login";
					$this->session->set_userdata($sess_data);
				}
				if($this->session->userdata('level')=='administrator'){
				  redirect(base_url("Administrator"));	
				}else{
					//echo "login";
				  //redirect(base_url("cadmin"));
				}
			}else{
				$data['statuslogin']='0';
				$this->load->view('login',$data);
			}
		}
		function logout(){
			$this->session->sess_destroy();
			redirect(base_url('Login'));
		}	
	
	}
?>