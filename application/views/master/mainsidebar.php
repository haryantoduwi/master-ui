  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php  echo base_url();?>asset/dist/img/avatar6.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo ucwords($this->session->userdata('nama'))?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Administrator-Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="#"><i class="fa fa-home"></i> <span>Home</span></a></li>
        <li class=""><a href="#"><i class="fa fa-bar-chart"></i> <span>Penjualan</span></a></li>
        <li class=""><a href="#"><i class="fa fa-bar-chart"></i> <span>Pendaftaran</span></a></li>
        <li class=""><a href="#"><i class="fa fa-bar-chart"></i> <span>Pengumuman</span></a></li>
        <li class=""><a href="#"><i class="fa fa-bar-chart"></i> <span>Rekap Data HER</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Cetak</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="#"><i class="fa fa-circle-o"></i> Data Lengkap Maba</a></li>
            <li class="active"><a href="#"><i class="fa fa-circle-o"></i> HER Maba</a></li>
          </ul>
        </li>
        <li class="hide"><a   href="<?php echo base_url('cuser/lihatdatapegawai')?>"><i class="fa fa-user"></i> <span>Daftar Pegawai</span></a></li>
        <li class="treeview hide">
          <a href="#">
            <i class="fa fa-user"></i> <span>Daftar Pegawai</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('cadmin/addpegawai')?>"><i class="fa fa-user"></i> <span>Add Pegawai</span></a></li>
            <li><a href="<?php echo base_url('cadmin/lihatpegterdaftar')?>"><i class="fa fa-group"></i> <span>Lihat</span></a></li>
          </ul>
        </li>
        <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABEL</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
